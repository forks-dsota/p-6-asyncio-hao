import json
import asyncio
import sdp_transform


class EchoServerProtocol:
    def connection_made(self, transport):
        self.transport = transport

    def datagram_received(self, data, addr):
        udp = data.decode()
        print('Received %r from %s' % (udp, addr))
        udp = json.loads(udp)
        udp['sdp'] = sdp_transform.parse(udp['sdp'])
        udp['sdp']['media'][0]["port"] = 34543
        udp['sdp']['media'][1]["port"] = 34543
        udp['sdp'] = sdp_transform.write(udp['sdp'])
        udp['type'] = "answer"
        udp_json = json.dumps(udp)
        print('Send %r to %s' % (udp, addr))
        self.transport.sendto(udp_json.encode(), addr)


async def main():
    print("Starting UDP server")

    # Get a reference to the event loop as we plan to use
    # low-level APIs.
    loop = asyncio.get_running_loop()

    # One protocol instance will be created to serve all
    # client requests.
    transport, protocol = await loop.create_datagram_endpoint(
        lambda: EchoServerProtocol(),
        local_addr=('127.0.0.1', 9999))

    try:
        await asyncio.sleep(3600)  # Serve for 1 hour.
    finally:
        transport.close()


asyncio.run(main())
